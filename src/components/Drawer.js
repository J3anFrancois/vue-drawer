export default {
  data() {
    return {
      height: 0,
      opened: false,
      closed: false,
      opening: false,
      closing: false,
      _contents: null,
      animationDuration: 0,
    };
  },
  created() {
    this.opened = this.$attrs.hasOwnProperty('opened');
    this.closed = ! this.opened;
  },
  mounted() {
    this.$nextTick(() => {
      this._contents = this.$el.querySelector('.drawer-contents');
      this.setAnimationDuration();
    });
  },
  computed: {
    isOpened() {
      return this.opened;
    },
    isClosed() {
      return this.closed;
    },
    isOpening() {
      return this.opening;
    },
    isClosing() {
      return this.closing;
    },
  },
  methods: {
    toggle() {
      if (this.isOpened) {
        this.close();
      } else {
        this.open();
      }
    },
    open() {
      this.closed = false;
      this.opened = false;
      this.opening = true;

      setTimeout(() => {
        this.opened = true;
        this.closed = ! this.opened;
        this.opening = false;
      }, this.animationDuration);
    },
    close() {
      this.closed = false;
      this.opened = false;
      this.closing = true;

      setTimeout(() => {
        this.opened = false;
        this.closed = ! this.opened;
        this.closing = false;
      }, this.animationDuration);
    },
    getCssProperty(key) {
      return getComputedStyle(this.$el).getPropertyValue(key);
    },
    setAnimationDuration() {
      const duration = this.getCssProperty('--drawer-animation-duration');
      this.animationDuration = duration.replace(/\D/g,'');
    },
  }
}
